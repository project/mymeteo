meteoit.module genera l'html del servizio fornito da ilmeteo.it (versione gratuita).

INSTALLAZIONE
- Carica il modulo meteoit.module nella cartella /sites/all/modules
- Abilita il modulo in /admin/build/modules

CONFIGURAZIONE
- Portati nell'elenco blocchi (/admin/build/block) e abilita il blocco "Previsioni del tempo"
- Clicca su configura
- Dai un titolo al blocco
- Scegli se mostrare le previsioni direttamente nel blocco o se caricarle in una popup. Tieni conto che se scegli di caricare le previsioni direttamente nel blocco si carica nello stesso una tabella larga 500px.
- Se vuoi puoi anche aggiungere una voce alla navigazione del tuo sito e far linkare tale voce alla pagina meteoit. Esempio: www.miostio.it/meteoit visualizzerˆ una pagina in cui vengono caricate le previsioni fornite da ilmeteo.it

---
nicola sanino - nicolasanino@criterianet.net